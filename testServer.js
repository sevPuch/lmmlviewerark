var http = require('http');

JSON_OUT = {
description: "fracture",
tissue: "bone"
}

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(JSON_OUT));
}).listen(8000);
