For testing the functionality of getting a JSON file from a server :
This should be implemented in LmmlViewer::slotOpenJSONFile(std::string url) 

For example :
    For running the server provided as testServer.js
        node testServer.js

    For getting the JSON file content
        curl http://localhost:8000