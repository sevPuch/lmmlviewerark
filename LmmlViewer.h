#ifndef INTERACTING_H
#define INTERACTING_H

class LmmlViewer
{
  public:
    // Variables :
    string jsonFileName;
    string CT_FileName;
    std::vector<std::map<int, double>> sliders;

    // VTK variables :
    vtkSmartPointer<vtkRenderer> ren;
    vtkSmartPointer<vtkRenderWindow> renWin;
    vtkSmartPointer<vtkRenderWindowInteractor> iren;
    vtkSmartPointer<vtkNamedColors> colors;
    vtkSmartPointer<vtkVolume> volume;
    vtkSmartPointer<vtkColorTransferFunction> volumeColor;
    vtkSmartPointer<vtkPiecewiseFunction> volumeScalarOpacity;
    vtkSmartPointer<vtkPiecewiseFunction> volumeGradientOpacity;
    vtkSmartPointer<vtkFixedPointVolumeRayCastMapper> volumeMapper;
    vtkSmartPointer<vtkVolumeProperty> volumeProperty;
    vtkSmartPointer<vtkMetaImageReader> reader;
    vtkSmartPointer<vtkCamera> camera;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> style;

    // Methods :
    std::vector<std::map<int, double>> initialiseSliders(std::vector<std::map<int, double>> sliders);
    void ReadImage(std::string _imagePath);
    void UpdateRenderer(std::map<int, double> _changedSlider);
    std::string slotOpenImageryFile();
    std::map<int, double> LmmlViewer::sliderCallback();
};

#endif