#include "LmmlViewer.h"

#include <type_traits>
#include <typeinfo>
#include <vector>
#include <map>
#include <sstream>
#include <iostream>
#include <iterator>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/optional.hpp>

#include <vtkCamera.h>
#include <vtkColorTransferFunction.h>
#include <vtkFixedPointVolumeRayCastMapper.h>
#include <vtkMetaImageReader.h>
#include <vtkNamedColors.h>
#include <vtkPiecewiseFunction.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkMarchingCubes.h>
#include <vtkClipVolume.h>

#include <vtkPlaneSource.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>

#include <vtkSphereSource.h>
#include <vtkProbeFilter.h>
#include <vtkSphere.h>
#include <vtkClipDataSet.h>
#include <vtkImplicitVolume.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLookupTable.h>

#include <vtkPlane.h>
#include <vtkVersion.h>
#include <vtkRendererCollection.h>
#include <vtkIdTypeArray.h>
#include <vtkTriangleFilter.h>
#include <vtkCommand.h>

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkCellPicker.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>

#include <array>
#include <vtkObjectFactory.h>
#include <vtkPolyData.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkFollower.h>
#include <vtkVectorText.h>
#include <vtkLineSource.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkSliderWidget.h>
#include <vtkPolyDataMapper.h>
#include <vtkCommand.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>

// Constructor
LmmlViewer::LmmlViewer()
{
    // Initialise VTK variables :
    this->ren = vtkSmartPointer<vtkRenderer>::New();
    this->renWin = vtkSmartPointer<vtkRenderWindow>::New();
    this->iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
    this->colors = vtkSmartPointer<vtkNamedColors>::New();
    this->volume = vtkSmartPointer<vtkVolume>::New();
    this->volumeScalarOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
    this->volumeGradientOpacity = vtkSmartPointer<vtkPiecewiseFunction>::New();
    this->volumeColor = vtkSmartPointer<vtkColorTransferFunction>::New();
    this->volumeMapper = vtkSmartPointer<vtkFixedPointVolumeRayCastMapper>::New();
    this->volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();
    this->reader = vtkSmartPointer<vtkMetaImageReader>::New();
    this->camera = vtkSmartPointer<vtkCamera>::New();
    this->style = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();

    // Call this->sliders = LmmlViewer::initialiseSliders(sliders);
}

// Deconstructor
LmmlViewer::~LmmlViewer()
{
    // vtkSmartPointers already take care of this.
    // No need to develop this.
}

// Called by main()
// This methods initialises all the sliders of the user interface.
std::vector<std::map<int, double>> LmmlViewer::initialiseSliders(std::vector<std::map<int, double>>)
{
    // Get all the sliders of the user interface here as a vector of map<int, double>, 
    // where int: slider_id and double: slider_value
}

// Called by clicking on "Open CT image file..." on the user interface
std::string LmmlViewer::slotOpenImageryFile()
{
    // Open file explorer, let the user choose a CT data .mhd file.
    // Copy the file's path to a std::string imagePath
    // Use that string as parameter for LmmlViewer::ReadImage(imagePath)

    /*    
    std::string imagePath;
    LmmlViewer::ReadImage(imagePath);
    */
}

// This method has to be called by LmmlViewer::slotOpenImageryFile()
// Reads a .mhd file and initialises a volume rendering scene.
void LmmlViewer::ReadImage(std::string _imagePath)
{
    // This method is called slotOpenImageryFile() with the path to CT image as parameter.
    // Set CT image path (data located in "../Data/FullHead.mhd").
    this->CT_FileName = _imagePath;

    // Reader of the CT data image
    const char *CT_cstr = CT_FileName.c_str();
    cout << CT_cstr << endl;
    reader->SetFileName(CT_cstr);
    reader->Update();

    // Rendering colors set up :
    std::array<unsigned char, 4> bkg{{230, 230, 230, 230}};
    this->colors->SetColor("BkgColor", bkg.data());

    // Rendering window and interactor setting :
    this->renWin->AddRenderer(ren);
    this->iren->SetRenderWindow(renWin);

    // The volume will be displayed by ray-cast alpha compositing.
    this->volumeMapper->SetInputConnection(reader->GetOutputPort());

    // Gradient opacity set up (enhances the image)
    this->volumeGradientOpacity->AddPoint(0, 0.0);
    this->volumeGradientOpacity->AddPoint(50, 0.2);
    this->volumeGradientOpacity->AddPoint(100, 1.0);

    // Assign various properties to the volume
    this->volumeProperty->SetColor(this->volumeColor);
    this->volumeProperty->SetScalarOpacity(this->volumeScalarOpacity);
    this->volumeProperty->SetGradientOpacity(this->volumeGradientOpacity);
    this->volumeProperty->ShadeOn();
    this->volumeProperty->SetAmbient(0.4);
    this->volumeProperty->SetDiffuse(0.6);
    this->volumeProperty->SetSpecular(0.5);

    // Use these opacity points for this work, it will be done automatically afterwards.
    this->volumeScalarOpacity->AddPoint(1000, 0.6);
    this->volumeColor->AddRGBPoint(1000, 0.9, 0.7, 0.6);
    this->volumeScalarOpacity->AddPoint(1800, 1);
    this->volumeColor->AddRGBPoint(1800, 1, 1, 0.9);

    // The vtkVolume is a vtkProp3D (like a vtkActor) and controls the position
    // and orientation of the volume in world coordinates.
    this->volume->SetMapper(this->volumeMapper);
    this->volume->SetProperty(this->volumeProperty);

    // Finally, add the volume to the renderer
    this->ren->AddViewProp(this->volume);

    // Set up the camera
    this->camera = this->ren->GetActiveCamera();
    this->camera->SetViewUp(0, 0, -1);
    double *c = this->volume->GetCenter();
    this->camera->SetPosition(c[0], c[1] - 400, c[2]);
    this->camera->SetFocalPoint(c[0], c[1], c[2]);
    this->camera->Elevation(30.0);

    // Assign the rendew window
    this->iren->SetRenderWindow(this->renWin);

    // Set up the interactor
    this->style->SetCurrentRenderer(this->ren);
    this->style->SetDefaultRenderer(this->ren);

    // Set a background color for the renderer
    this->ren->SetBackground(this->colors->GetColor3d("BkgColor").GetData());

    // Set the size of the render window
    // Initialise interactor
    this->renWin->SetSize(640, 480);
    this->ren->Render();
    this->iren->Initialize();
    this->iren->Start();
    
    return;
}

// Called when a slider's value has been changed on the user interface
std::map<int, double> LmmlViewer::sliderCallback()
{
    // Stores changed slider's id as an int, slider's value as a double
    // Stores those two values into a map
    // Call LmmlViewer::UpdateRenderer(changedSlider) with the map as parameter.

    /*
    int sliderID;
    double sliderValue;
    std::map<int, double> changedSlider;
    LmmlViewer::UpdateRenderer(changedSlider);
    return changedSlider;
    */
}

// Updates opacities of the volume in the renderer when a slider's value has been changed.
// This method has to be called by the slider callback method: LmmlViewer::sliderCallback() 
// 
// Leave this method empty.
void LmmlViewer::UpdateRenderer(std::map<int, double> _changedSlider)
{
    for(std::vector<std::map<int, double>>::iterator it = this->sliders.begin(); it!=this->sliders.end(); it++)
    {
        // Change opacities following slider values.
        // ...
    }
}